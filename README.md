# brutforce



## Getting started
To start clone this project with https://gitlab.com/MatthieuBrehamel/brutforce.git

You need to run ```npm install```

Once it's done you can run ```node bruteforce.js ['type'] ['option1'] ['option2']```

You can choose between two type 'dico' or 'force'

If you use 'force' type ```node bruteforce.js force ['minimum length'] ['maximum length']``` exemple : ```node bruteforce.js force 2 4```

If you use 'dico' type ```node bruteforce.js dico```


## Resume

This program is use to force the password of website with two mode.

The Dico mode read file and test all password 

The force mode create an array with all combinaison and test it.

## Technology
- Node Js 
- puppeteer
- chrome-aws-lambda
